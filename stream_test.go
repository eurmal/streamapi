/*
   This file is part of StreamAPI.
   StreamAPI is free software: you can redistribute it and/or modify it under the terms
   of the GNU Affero General Public License as published by the Free Software Foundation,
   either version 3 of the License, or (at your option) any later version.
   StreamAPI is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
   without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU Affero General Public License for more details.
   You should have received a copy of the GNU Affero General Public License along
   with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package stream

import (
	"cmp"
	"context"
	"fmt"
	"github.com/stretchr/testify/suite"
	"math/rand/v2"
	"slices"
	"testing"
	"time"
)

type StreamTestSuite struct {
	suite.Suite
}

func TestStreamTestSuite(t *testing.T) {
	suite.Run(t, new(StreamTestSuite))
}

func (s *StreamTestSuite) TestFilter() {
	input := []int{5, 6, 2, 1, 10, 11, 4}
	ctx := context.Background()
	predicate := func(i int) bool {
		return i > 5
	}

	output := ToStream(ctx, input).
		Filter(predicate).
		Collect()

	s.Equal(3, len(output))
	s.ElementsMatch([]int{6, 10, 11}, output)
}

func (s *StreamTestSuite) TestMap() {
	input := []string{"hello", "goodbye"}
	ctx := context.Background()
	mapperFunc := func(s string) string {
		return fmt.Sprintf("%s world", s)
	}

	output := ToStream(ctx, input).
		Map(mapperFunc).
		Collect()

	s.Equal(2, len(output))
	s.ElementsMatch([]string{"hello world", "goodbye world"}, output)
}

func (s *StreamTestSuite) TestForEach() {
	input := []string{"hello", "goodbye"}
	ctx := context.Background()

	timesCalled := 0
	forEachFunc := func(s string) {
		timesCalled++
	}

	ToStream(ctx, input).
		ForEach(forEachFunc)

	s.Equal(2, timesCalled)
}

func (s *StreamTestSuite) TestDistinct() {
	input := []int{2, 3, 50, 9, 1, 50}

	output := ToStream(context.Background(), input).
		Distinct().
		Collect()

	s.Equal(5, len(output))
	s.ElementsMatch([]int{2, 3, 50, 9, 1}, output)
}

func (s *StreamTestSuite) TestAnyMatchOK() {
	input := []int{2, 3}
	predicate := func(i int) bool {
		return i == 3
	}

	output := ToStream(context.Background(), input).
		AnyMatch(predicate)

	s.True(output)
}

func (s *StreamTestSuite) TestAnyMatchNOK() {
	input := []int{2, 3}
	predicate := func(i int) bool {
		return i == 6
	}

	output := ToStream(context.Background(), input).
		AnyMatch(predicate)

	s.False(output)
}

func (s *StreamTestSuite) TestAllMatchOK() {
	input := []int{2, 2, 2}
	predicate := func(i int) bool {
		return i == 2
	}

	output := ToStream(context.Background(), input).
		AllMatch(predicate)

	s.True(output)
}

func (s *StreamTestSuite) TestAllMatchNOK() {
	input := []int{2, 2, 3, 2}
	predicate := func(i int) bool {
		return i == 2
	}

	output := ToStream(context.Background(), input).
		AllMatch(predicate)

	s.False(output)
}

func (s *StreamTestSuite) TestNoneMatchOK() {
	input := []int{2, 2, 2}
	predicate := func(i int) bool {
		return i == 3
	}

	output := ToStream(context.Background(), input).
		NoneMatch(predicate)

	s.True(output)
}

func (s *StreamTestSuite) TestNoneMatchNOK() {
	input := []int{2, 2, 3, 2}
	predicate := func(i int) bool {
		return i == 3
	}

	output := ToStream(context.Background(), input).
		NoneMatch(predicate)

	s.False(output)
}

func (s *StreamTestSuite) TestConcat() {
	input := []string{"trans", "lives"}
	appendInput1 := ToStream(context.Background(), []string{"matter"})
	appendInput2 := ToStream(context.Background(), []string{"!"})

	output := ToStream(context.Background(), input).
		Concat(appendInput1, appendInput2).
		Collect()

	s.Len(output, 4)
	s.ElementsMatch([]string{"trans", "lives", "matter", "!"}, output)
}

func (s *StreamTestSuite) TestSort() {
	type thing struct {
		S string
		I int
	}

	input := []thing{
		{
			S: "xyz",
			I: 123,
		},
		{
			S: "abc",
			I: 999,
		},
		{
			S: "hjk",
			I: -50,
		},
	}

	sortFunc1 := func(a, b thing) int {
		return cmp.Compare(a.S, b.S)
	}

	sortFunc2 := func(a, b thing) int {
		return cmp.Compare(a.I, b.I)
	}

	output := ToStream(context.Background(), input).
		Sort(sortFunc1).
		Collect()

	s.Equal(len(input), len(output))
	s.Equal(input[1], output[0])
	s.Equal(input[2], output[1])
	s.Equal(input[0], output[2])

	output = ToStream(context.Background(), input).
		Sort(sortFunc2).
		Collect()

	s.Equal(len(input), len(output))
	s.Equal(input[2], output[0])
	s.Equal(input[0], output[1])
	s.Equal(input[1], output[2])
}

func (s *StreamTestSuite) TestReverse() {
	input := []int{1, 2, 3, 4, 5}

	output := ToStream(context.Background(), input).
		Reverse().
		Collect()

	s.ElementsMatch([]int{5, 4, 3, 2, 1}, output)
}

func (s *StreamTestSuite) TestLimit() {
	input := []string{"hello", "goodbye", "good evening"}
	ctx := context.Background()

	output := ToStream(ctx, input).
		Limit(2).
		Collect()

	s.Equal(2, len(output))
	s.ElementsMatch([]string{"hello", "goodbye"}, output)
}

func (s *StreamTestSuite) TestSkip() {
	input := []string{"hello", "goodbye", "good evening"}
	ctx := context.Background()

	output := ToStream(ctx, input).
		Skip(2).
		Collect()

	s.Equal(1, len(output))
	s.ElementsMatch([]string{"good evening"}, output)
}

func (s *StreamTestSuite) TestSkipAll() {
	input := []string{"hello", "goodbye", "good evening"}
	ctx := context.Background()

	output := ToStream(ctx, input).
		Skip(3).
		Collect()

	s.Equal(0, len(output))
}

func (s *StreamTestSuite) TestReduceSum() {
	input := []int{1, 2, 3, 4, 5}
	ctx := context.Background()

	accumulator := func(i, j int) int {
		return i + j
	}

	output := ToStream(ctx, input).
		Reduce(accumulator)

	s.Equal(15, output)
}

func (s *StreamTestSuite) TestReduceStringConcat() {
	input := []string{"strawberries", " are", " tasty"}
	ctx := context.Background()

	accumulator := func(i, j string) string {
		return i + j
	}

	output := ToStream(ctx, input).
		Reduce(accumulator)

	s.Equal("strawberries are tasty", output)
}

func (s *StreamTestSuite) TestPerformance() {
	fmt.Println("Input 100000 random integers between [0,10000), return a distinct reverse sorted list\n where each value has 5 added to it, skipping the first 50 and last 500 elements,\n as well as filtering out any value below 1001. Averaged over 100 cycles.")
	var totalWith float64
	var totalWithout float64

	type bestCycle struct {
		With       time.Duration
		Without    time.Duration
		Difference time.Duration
	}

	bc := &bestCycle{}
	for c := 0; c < 100; c++ {
		var input []int
		for i := 0; i < 100000; i++ {
			input = append(input, rand.IntN(10000))
		}

		// StreamAPI
		streamStart := time.Now()
		streamOutput := ToStream(context.Background(), input).
			Skip(50).
			Limit(90500).
			Distinct().
			Map(func(i int) int { return i + 5 }).
			Filter(func(i int) bool { return i > 1000 }).
			Sort(func(i, j int) int { return cmp.Compare(i, j) }).
			Reverse().
			Collect()
		streamDuration := time.Since(streamStart)

		// Without StreamAPI
		withoutStart := time.Now()
		m := make(map[int]bool)
		for i, k := range input {
			if i < 50 {
				continue
			}
			if i > 90550 {
				break
			}
			k = k + 5
			if k <= 1000 {
				continue
			}
			m[k] = true
		}
		var withoutOutput []int
		for k := range m {
			withoutOutput = append(withoutOutput, k)
		}
		slices.Sort(withoutOutput)
		slices.Reverse(withoutOutput)
		withoutDuration := time.Since(withoutStart)

		s.Equal(len(withoutOutput), len(streamOutput))
		s.ElementsMatch(withoutOutput, streamOutput)

		totalWith += float64(streamDuration.Nanoseconds())
		totalWithout += float64(withoutDuration.Nanoseconds())

		if bc.Difference > withoutDuration-streamDuration {
			bc = &bestCycle{
				With:       streamDuration,
				Without:    withoutDuration,
				Difference: streamDuration - withoutDuration,
			}
		}

	}

	if totalWith > totalWithout {
		fmt.Printf("Stream API is about %.2f times slower\n", totalWith/totalWithout)
	} else {
		fmt.Printf("Stream API is about %.2f times faster\n", totalWithout/totalWith)
	}

	fmt.Printf("Best cycle had stream at %s and without stream at %s for a difference of %s", bc.With, bc.Without, bc.Difference)
}
