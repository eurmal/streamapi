![stream gopher](gopher.png)

# StreamAPI
StreamAPI is a go library inspired by the Java stream API.

## Supported functionality

### ToStream
Converts a slice of type T to a stream of the same type.

### Collect
Converts a stream of type T to a slice of the same type.

### Filter
Given a predicate, returns a stream of elements fulfilling it.

### Map
Takes a function and applies it on each element of a stream, then returns the stream.(Limitation, atm it does not
support changing types, so for example using Atoi as the function will not work).

### Distinct
Returns a stream of the same type but without duplicates.

### ForEach
Passes each element of a stream to a given function.

### AnyMatch
Returns true if any element of a stream fulfills the supplied predicate.

### AllMatch
Returns true if all elements of a stream fulfills the supplied predicate.

### NoneMatch
Returns true if no elements of a stream fulfills the supplied predicate.

### Concat
Appends the elements from the supplied stream(s) to a stream.

### Sort
Sorts a stream using the supplied sorting function and returns the sorted stream.

### Reverse
Reverses the order of a stream.

### Limit
Returns a stream of the same type of the first *n* elements where n is a supplied limit.

### Skip
Returns a stream of the same type but skipping the first *n* elements.

### Reduce
Provides basic (not fully featured) reduce functionality on a stream. For example summing or
string concatenation.

## Example usage
```go
	input := []int{5, 6, 2, 1, 10, 11, 4, 4, 6}
	ctx := context.Background()
	predicate := func(i int) bool {
		return i > 5
	}

	output := ToStream(ctx, input).
		Filter(predicate).
		Distinct().
		Collect()

	// Returns a slice containing [6, 10, 11]
```

## Disclaimer
This library is not fit for production use at the moment, *many* shortcuts have been taken with regard to edge cases
and error handling. You can very easily hang yourself with the amount of rope it gives you.

Performance is also... not good. A simple comparison (test available in the `stream_test.go`) shows that StreamAPI is
about 22 times slower than using regular go functionality.

## License
This library is licensed under the AGPLv3 (or later) license, the full license is available in the COPYING file.
