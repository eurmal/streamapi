/*
   This file is part of StreamAPI.
   StreamAPI is free software: you can redistribute it and/or modify it under the terms
   of the GNU Affero General Public License as published by the Free Software Foundation,
   either version 3 of the License, or (at your option) any later version.
   StreamAPI is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
   without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU Affero General Public License for more details.
   You should have received a copy of the GNU Affero General Public License along
   with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package stream

import (
	"context"
	"slices"
)

type Stream[T comparable] struct {
	ctx    context.Context
	stream chan T
}

func ToStream[T comparable](ctx context.Context, in []T) *Stream[T] {
	stream := &Stream[T]{
		ctx:    ctx,
		stream: make(chan T),
	}
	go func() {
		defer close(stream.stream)
		for _, element := range in {
			select {
			case <-stream.ctx.Done():
				return
			case stream.stream <- element:
			}
		}
	}()
	return stream
}

func (s *Stream[T]) Filter(predicate func(element T) bool) *Stream[T] {
	outStream := &Stream[T]{
		ctx:    s.ctx,
		stream: make(chan T),
	}
	go func() {
		defer close(outStream.stream)
		for element := range s.stream {
			if predicate(element) {
				select {
				case <-s.ctx.Done():
					return
				case outStream.stream <- element:
				}
			}
		}

	}()
	return outStream
}

func (s *Stream[T]) Map(mapper func(element T) T) *Stream[T] {
	outStream := &Stream[T]{
		ctx:    s.ctx,
		stream: make(chan T),
	}
	go func() {
		defer close(outStream.stream)
		for element := range s.stream {
			select {
			case <-s.ctx.Done():
				return
			default:
				outStream.stream <- mapper(element)
			}
		}

	}()
	return outStream
}

func (s *Stream[T]) Distinct() *Stream[T] {
	outStream := &Stream[T]{
		ctx:    s.ctx,
		stream: make(chan T),
	}
	go func() {
		defer close(outStream.stream)
		distinctMap := make(map[T]bool)
		for element := range s.stream {
			select {
			case <-s.ctx.Done():
				return
			default:
				distinctMap[element] = true
			}
		}
		for element := range distinctMap {
			outStream.stream <- element
		}

	}()
	return outStream
}

func (s *Stream[T]) Collect() []T {
	var out []T
	for element := range s.stream {
		select {
		case <-s.ctx.Done():
			return out
		default:
			out = append(out, element)
		}
	}
	return out
}

func (s *Stream[T]) ForEach(action func(element T)) {
	for element := range s.stream {
		select {
		case <-s.ctx.Done():
			return
		default:
			action(element)
		}
	}
}

func (s *Stream[T]) AnyMatch(predicate func(element T) bool) bool {
	for element := range s.stream {
		select {
		case <-s.ctx.Done():
			return false
		default:
			if predicate(element) {
				return true
			}
		}
	}
	return false
}

func (s *Stream[T]) AllMatch(predicate func(element T) bool) bool {
	for element := range s.stream {
		select {
		case <-s.ctx.Done():
			return false
		default:
			if !predicate(element) {
				return false
			}
		}
	}
	return true
}

func (s *Stream[T]) NoneMatch(predicate func(element T) bool) bool {
	for element := range s.stream {
		select {
		case <-s.ctx.Done():
			return false
		default:
			if predicate(element) {
				return false
			}
		}
	}
	return true
}

func (s *Stream[T]) Concat(sn ...*Stream[T]) *Stream[T] {
	outStream := &Stream[T]{
		ctx:    s.ctx,
		stream: make(chan T),
	}

	go func() {
		defer close(outStream.stream)
		for element := range s.stream {
			select {
			case <-s.ctx.Done():
				return
			default:
				outStream.stream <- element
			}
		}
		for _, sx := range sn {
			for element := range sx.stream {
				select {
				case <-sx.ctx.Done():
					return
				default:
					outStream.stream <- element
				}
			}
		}
	}()
	return outStream
}

func (s *Stream[T]) Sort(sorter func(i, j T) int) *Stream[T] {
	outStream := &Stream[T]{
		ctx:    s.ctx,
		stream: make(chan T),
	}
	go func() {
		defer close(outStream.stream)
		var elements []T
		for element := range s.stream {
			select {
			case <-s.ctx.Done():
				return
			default:
				elements = append(elements, element)
			}
		}

		slices.SortStableFunc(elements, sorter)
		for _, element := range elements {
			outStream.stream <- element
		}
	}()
	return outStream
}

func (s *Stream[T]) Reverse() *Stream[T] {
	outStream := &Stream[T]{
		ctx:    s.ctx,
		stream: make(chan T),
	}
	go func() {
		defer close(outStream.stream)
		var elements []T
		for element := range s.stream {
			select {
			case <-s.ctx.Done():
				return
			default:
				elements = append(elements, element)
			}
		}

		slices.Reverse(elements)
		for _, element := range elements {
			outStream.stream <- element
		}
	}()
	return outStream
}

func (s *Stream[T]) Limit(limit uint) *Stream[T] {
	outStream := &Stream[T]{
		ctx:    s.ctx,
		stream: make(chan T),
	}
	go func() {
		defer close(outStream.stream)
		var i uint
		for element := range s.stream {
			select {
			case <-s.ctx.Done():
				return
			default:
				if i < limit {
					outStream.stream <- element
					i++
				} else {
					break
				}
			}
		}

	}()
	return outStream
}

func (s *Stream[T]) Skip(skip uint) *Stream[T] {
	outStream := &Stream[T]{
		ctx:    s.ctx,
		stream: make(chan T),
	}
	go func() {
		defer close(outStream.stream)
		var i uint
		for element := range s.stream {
			select {
			case <-s.ctx.Done():
				return
			default:
				if i < skip {
					i++
					continue
				} else {
					outStream.stream <- element
				}
			}
		}

	}()
	return outStream
}

func (s *Stream[T]) Reduce(accumulator func(acc, element T) T) T {
	var out T
	for element := range s.stream {
		select {
		case <-s.ctx.Done():
			return out
		default:
			out = accumulator(out, element)
		}
	}
	return out
}
